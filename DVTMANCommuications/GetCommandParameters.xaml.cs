﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DVTMANCommuications;

namespace DVTMANCommuications.CommandParameters
{
    /// <summary>
    /// Interaction logic for GetCommandParameters.xaml
    /// </summary>
    public partial class GetCommandParameters : Window
    {
        List<parameterDefs> cmdParams = new List<parameterDefs>();
        public commandClass cmdDef = new commandClass();

        private int[] map;

        bool doneClicked = false;

        //       public GetCommandParameters(List<parameterDefs> defs)
        public GetCommandParameters(commandClass defs)
        {
            InitializeComponent();
            cmdDef = defs;
            if (defs.Parameters == null)     // if no parameters
                return;

            // make a list of only the input parameters
            int j = 0;
            int i;
            Array.Resize(ref map, cmdDef.Parameters.Count);
            for (i = 0; i < cmdDef.Parameters.Count; i++)
            {
                if (cmdDef.Parameters[i].ReturnValue)
                {
                    map[j++] = i;           // save location to put it back
                }
                else
                {
                    cmdParams.Add(cmdDef.Parameters[i]);    // set up to get data
                }
            }
            Array.Resize(ref map, j);
            //            this.cmdParamList.ItemsSource = cmdParams;
            this.cmdParamList.ItemsSource = cmdParams;
            this.DataContext = cmdParams;

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            if (map.Length != 0)
            {
                // put the results back in the data structure
                int j = 0;
                for (int i = 0; i < map.Length; i++)
                {
                        cmdDef.Parameters[map[i]].ParaData = cmdParams[i].ParaData;
                }
            }
        }

        private void Button_Click_SendCommand(object sender, RoutedEventArgs e)
        {
            foreach (parameterDefs item in cmdParamList.Items)
            {
                MessageBox.Show(item.ParaData);
            }
        }
    }

}
