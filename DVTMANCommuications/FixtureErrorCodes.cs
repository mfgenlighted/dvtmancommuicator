﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DVTMANCommuications
{
    public class FixtureErrorCodes
    {

        //public const int ComponetInitFail = -6000;                  // fixture componet failed to initialize
        //public const int MoreThanOneInstrDefined = -6001;           // can not have LabJack and A34970A enabled at the same time
        //public const int VisaSendError = -6002;                     // got a visa error trying to send visa instrument command
        public const int ChannelNotDefined = -6001;                     // The specified channel is not defined
        public const int InstrumentNotConnected = -6003;               // instrument is not connected
        //public const int VisaReadError = -6004;                     // got a visa error reading a instrument
        //public const int InstrumentReturnedBadFormat = -6005;       // the data returned from the instrument was not in the correct format
        //public const int InvalidOutputType = -6006;                 // the channel type define did not match the ouput type of the instrument
        //public const int VisaOpenError = -6007;                     // error opening the visa port
        //public const int NotCorrectInstrument = -6008;              // was no the instrument expected
        //public const int NotCorrectCard = -6009;                    // not expected card in the instrument
        public const int MissingInstrumentOption = -6010;           // a option in the instrument is not present
        //public const int InstrumentErrorMsg = -6011;                // found a error message in a instrument
        public const int StationConfigNotRead = -6012;              // station configuration must be read before trying to intialize fixture
        public const int InvalidChannelData = -6013;                // the channel data define has invalid data
        public const int InvalidInstrumentDefined = -6014;          // the instrument in the definition is not on this station
        //public const int InstrumentReadError = -6015;              // non visa instrument read error
        //public const int LabjackConnectError = -6016;               // error trying to connect to the labjack
        //public const int LabjackSetupError = -6017;                 // error trying to setup the labjack
        //public const int RelayReadbackWrong = -6018;                // the readback of the relay after it was set is wrong
        //public const int RelayControlError = -6019;                 // error trying to control the relay
        public const int DUTConsoleOpenError = -6020;               // error opening the sensor console port
        public const int DUTConsoleNotOpen = -6021;                 // trying to use the sensor console port but it is not open
        public const int DUTConsoleWriteError = -6022;              // error writing to the sensor console
        public const int DUTConsoleCloseError = -6023;              // problem closing dut port
        public const int LEDDetectorNotInitialize = -6024;          // LED detector port has not been initialized
        public const int LEDDetectorNotOpen = -6025;                // LED detector port is not open
        //public const int SnifferPortNotInitialized = -6025;         // sniffer port is not initialized
        //public const int SnifferPortNotOpen = -6026;                // sniffer port is not open
        //public const int SnifferWriteError = -6027;                 // sniffer port write error
        //public const int InvalidParameter = -6028;                  // invalid parameter

        // any error from the ref radio is considered a system error
        public const int refRadioUnexpectedCmdReturn = -6029;                   // unexpected response to a command
        public const int refRadioCommandTimeout = -6030;                        // timeout when command was issued
        public const int refRadioCmdResponseStatusFailed = -6031;               // status response to a command was a fail
        public const int refRadioCmdResponsePayloadLenWrong = -6032;            // command response payload lenght does not match lenght in response stucture
        public const int refRadioCmdPacketFormatError = -6033;                  // format error in the command response packet
        public const int refRadioCmdResponseHeaderLenWrong = -6034;             // command response header lenght is wrong
        public const int refRadioCmdPacketCheckSumError = -6035;                // command packet checksum is wrong
        public const int refRadioCmdMsgTypeNotDVTMAN = -6036;                   // command response message type is not a DVTMAN message

        public const int FlasherBase = -6100;                       // base code for flasher errors. See Flasher.cs for defines

        public const int DAQBase = -6150;                           // base of DAQ errors. See FixturInstrBase for defines

    }

    public class DUTErrorCodes
    {
        // DUT console errors. com errors are a DUT error, not a system error
        public const int dutUnexpectedCmdReturn = 300;
        public const int dutCommandTimeout = 301;
        public const int dutCmdResponseStatusFailed = 302;
        public const int dutCmdResponsePayloadLenWrong = 303;
        public const int dutCmdPacketCheckSumError = 304;
        public const int dutCmdResponseHeaderLenWrong = 305;
        public const int dutCmdPacketFormatError = 306;
        public const int dutCmdMsgTypeNotDVTMAN = 307;
        public const int dutCmdInputDataFormatError = 308;
    }
}
