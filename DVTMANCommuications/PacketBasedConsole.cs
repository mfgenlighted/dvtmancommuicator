﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO.Ports;
using System.IO;
using System.Diagnostics;
using System.Collections.Concurrent;
using System.Windows;

/// <summary>
/// This is the sensor commuications using the packet structure.
/// </summary>

namespace DVTMANCommuications
{
    public class parameterDefs
    {
        public string ParaText { get; set; }
        public byte NumberOfBytes { get; set; }
        public string ParaData { get; set; }
        public bool ReturnValue { get; set; }       // set to true if this is a return value
    }

    public class commandClass
    {
        public string CommandText { get; set; }
        public bool Display { get; set; }
        public byte CmdId { get; set; }
        public string CommandFunctionName { get; set; }
        public List<parameterDefs> Parameters { get; set; }

    }

    public partial class MainWindow : Window
    {
        //------------------------
        // events
        //  Use UpdateStatusWindow(msg) to display a message on the
        //  MainWindow status window.
        // 
        // From the initializing routine, add
        //     sensor.StatusUpdated += new EventHandler<StatusUpdatedEventArgs>(xxxx);
        //  where xxxx is the routine to update the windows.
        //------------------------
        BlockingCollection<byte[]> sensorBlockData = new BlockingCollection<byte[]>();



        // locks
        static readonly object locker = new object();           // used to lock the response que
        static readonly object flocker = new object();          // used to lock the output file

        // error reporting
        public string LastErrorMessage = string.Empty;
        public int LastErrorCode = 0;
        public char[] LastPayload;

        // com defines
        public string PortName = string.Empty;
        public int BaudRate = 115200;                  // default to 115200

        // command parameters
        public int CommandRecieveTimeoutMS = 3000;      // number of ms to what for a response after a command
        public bool DisplayRcvChar = false;             // true= display recieved characters on screen
        public bool DisplaySendChar = false;              // true = display sent characters on screeen
        public bool IsThisDUTConsole = true;                // used for character display colors. true=dut false=sniffer

        private SerialPort serial = null;
        private int bufferSaveIndex = 0;                            // next location in the packet buffer to put data
        private byte[] packetBuffer = new byte[256];                // incoming packet buffer
        private List<byte[]> packetList = new List<byte[]>();       // this is a list of the packets recived.
        private string dumpFileName = string.Empty;                 // if file name present, dump data to file

        // transport values
        private byte packetTxSeqNumber = 0;                       // incremented after each packet transmit
        private byte packetRxSeqNumber = 0;                       // incremented after each packet recieve

        // queing
        ConcurrentQueue<byte[]> rcvDataQue = new ConcurrentQueue<byte[]>();

        // defined commands
        //
        //The Dvtman message consists of three parts, which are message type, message ID and parameters.
        //    1st byte: Message type/cateory for Dvtman messages, which differentiates Dvtman message type from other COBS message types.
        //    2nd byte: Message ID
        //    3rd byte and so on: Parameter list for the Dvtman command associated with the above message ID.
        // The table blow shows the messages defined for Dvtman.Notations used in the table are described as
        //    :P: param   B: bytes
        // The first byte in respond message is “stats”. The “stats” is  1 byte command status. 0 represents 
        //    success which means the command is received and executed successfully.
        //    If the “stats” is not 0, a non-zero stats shows the error code

        public const byte DVTMANMessageType = 0x57;
        public enum DVTMANCommandStatus
        {
            GOOD,
            BAD,
        }

        public enum DVTMANCommands
        {
            CMD_DISPLAY_MSG_VERSION = 0,    // display the current message version
            RSP_DISPLAY_MSG_VERSION,        // P1: stats  P2: version [1B]

            CMD_SET_NV_MAC,            // mac address for radio, ble and ethernet, P1: radio mac [6B, LSB]  P2: ble mac [6B, LSB]  P3: ble mac [6B, LSB]	
            RSP_SET_NV_MAC,             // P1: stats
            CMD_SET_NV_HW_CONFIG,       // default radio config, P1: pirSensorType[1B]  
            RSP_SET_NV_HW_CONFIG,       // P1: stats
            CMD_SET_NV_PCBA,            // set pcba config in nv flash, P1:  partNo [15B, char] P2:  serailNo [20B, char] 
            RSP_SET_NV_PCBA,
            CMD_SET_NV_HLA,             // set hla config in nv flash , P1: hlaPartNo [15B, char] P2: hlaSerialNo [20B, char] P3: hlaModelname [12B, char]
            RSP_SET_NV_HLA,
            CMD_SET_RADIO,              // set temporary radio configh
                                        //      P1: channelD [1B] P2: panID [2B, LSB hex] P3: txPower [1B]
                                        //      P4: encrypt_key [16B, char] P5: ttl [1B] P6: rate [1B data rate eg 16:1mbps] 
            RSP_SET_RADIO,
            CMD_SET_NV_CLEAR,           // clear non-violatile config, no param
            RSP_SET_NV_CLEAR,

            CMD_DISPLAY_VERSION,        // Image ID 
            RSP_DISPLAY_VERSION,        // P1: stats P2: image version ID[4B LSB]
            CMD_DISPLAY_BLE_MODE,       // current ble mode 
            RSP_DISPLAY_BLE_MODE,       // P1: stats P2: ble mode [1B]
            CMD_DISPLAY_NV_MAC,         // return both radio&ble 
            RSP_DISPLAY_NV_MAC,         // P1: stats P2: radio mac[6B] P3: ble mac[6B]
            CMD_DISPLAY_NV_PCBA,        // pcba config in nv flash
            RSP_DISPLAY_NV_PCBA,        // P1: stats P2:  partNo [15B] P3:  serailNo [20B]
            CMD_DISPLAY_NV_HLA,         // hla config in nv flash 
            RSP_DISPLAY_NV_HLA,         // P1: stats P2: hlaPartNo [15B, char] P3: hlaSerialNo [20B, char] P4: hlaModelname [12B, hex]
            CMD_DISPLAY_NV_HW_CONFIG,   // Get the hardware config 
            RSP_DISPLAY_NV_HW_CONFIG,   // variable data
            CMD_DISPLAY_RADIO,          // current radio param used 
            RSP_DISPLAY_RADIO,          // P1: stats P2: channelD [1B] P3: panID [2B, LSB hex] P4: txPower [1B]
                                        // P5: encrypt_key [16B, char] P6: ttl [1B] P7: dataRate [1B]

            CMD_PERFORM_POST,           // power on self test, do before change over, no param 
            RSP_PERFORM_POST,           // P1: status P2: crc saved[4B LSB] p3: crc calculated[4B LSB]
            CMD_PERFORM_CHANGE,         // change over to application image. Kona/kalapana app firmware might output stats or report profile through CU port 
            RSP_PERFORM_CHANGE,
            CMD_PERFORM_TEMP_CAL,       // temperature calibration. Input current room temperarture, P1: room temperature[1B, celsius] 
            RSP_PERFORM_TEMP_CAL, 		// P1: stats P2:raw temperature reading[2B] 
            CMD_PERFORM_DIM_VOLTAGE,    // specify dim1 voltage and dim2 voltage in range of 0 to 100(0~10v), P1: dim1 voltage[1B] P2: dim2 voltage[1B]
            RSP_PERFORM_DIM_VOLTAGE,    // P1: stats
            CMD_PERFORM_LED,            // red, green, blue 
            RSP_PERFORM_LED,
            CMD_PERFORM_SENSOR_READ,    // sensor read: P1:status, P2:pir[2B, 14bits], P3:ambient_lux[4B], P5:temperature[2B]
            RSP_PERFORM_SENSOR_READ,
            CMD_PERFORM_REBOOT,         // system reboot, no param 
            RSP_PERFORM_REBOOT,
            CMD_PERFORM_BLE_MODE,       // ble mode tests between two devices which can be programmed into idle, scan raw or beacon modes 
            RSP_PERFORM_BLE_MODE,
            CMD_PERFORM_WTEST,          // radio 802.15.4 test, P1: destination mac[6B] 
            RSP_PERFORM_WTEST,          // P1: stats P2: destination mac [6B] P3: destination LQI [1B] P4: source LQI  [1B
            CMD_DISPLAY_FLASH,          // P1:addr[4B, LSB]
            RSP_DISPLAY_FLASH,          // P1: stats, P2: data[16B]
            CMD_SET_DALI_INTERFACE,     // Command to set DALI interface type :
                                        //  DALI_INTERFACE_NONE = 0
                                        //  DALI_INTERFACE_MASTER = 1
                                        //  DALI_INTERFACE_SLAVE = 2
                                        // Note that the DUT should be set to a DALI master and the Golden Unit to a DALI slave.
            RSP_SET_DALI_INTERFACE,     // P1: stats
            CMD_PERFORM_DALI_TX_RX,     // no params
                                        // Command to initiate the DALI bus test. The DUT sends a 4-byte pattern to the Golden Unit.
                                        // The Golden Unit loops back each byte received.  See section 2.1 for a more detail description of this test.
            RSP_PERFORM_DALI_TX_RX,     // P1: stats stats:  0 on success, 1 on failure (no data received or data mismatch)	
            CMD_SET_FL_STATE,           // Command to set the fixtureless state. mode = 0(fixtured, i.e., not fixtureless), mode = 1(fixtureless)  P1: mode [1B]
            RSP_SET_FIXTURELESS_STATE,  // P1: stats
            CMD_DISPLAY_FL_STATE,       // Command to display the fixtureless state.  No Param
            RSP_DISPLAY_FL_STATE        // P1: stats, P2: state [1B] (0=fixtured, 1=fixtureless)
        }


    //public class parameterDefs
    //{
    //    public string ParaText { get; set; }
    //    public byte NumberOfBytes { get; set; }
    //}

    //public class commandClass
    //{
    //    public string CommandText { get; set; }
    //    public bool Display { get; set; }
    //    public byte CmdId { get; set; }
    //    public List<parameterDefs> Parameters { get; set; }

    //}

    public List<commandClass> Commmands = new List<commandClass>
        {
            new commandClass
            {
                CommandText = "Get Display Message Version",
                Display = true,
                CmdId = (byte)DVTMANCommands.CMD_DISPLAY_MSG_VERSION,
                CommandFunctionName = "GetImageID",
                Parameters = new List<parameterDefs>()
                {
                    new parameterDefs(){ParaText="Version", NumberOfBytes=1, ReturnValue=true}
                }
            },
            new commandClass
            {
                CommandText = "Get Display Message Version response",
                Display = false,
                CmdId = (byte)DVTMANCommands.RSP_DISPLAY_MSG_VERSION,
            },
            new commandClass
            {
                CommandText = "Set MAC 3 addresses",
                Display = true,
                CmdId = (byte)DVTMANCommands.CMD_SET_NV_MAC,
                CommandFunctionName = "SetMACData",
                Parameters = new List<parameterDefs>()
                {
                    new parameterDefs() {ParaText="radio mac", NumberOfBytes=6},
                    new parameterDefs() {ParaText="ble mac", NumberOfBytes=6}
                }
            },
            new commandClass
            {
                CommandText = "Set MAC addresses response",
                Display = false,
                CmdId = (byte)DVTMANCommands.RSP_SET_NV_MAC,
            },
            new commandClass
            {
                CommandText = "Set hardware configuration",
                Display = true,
                CmdId = (byte)DVTMANCommands.CMD_SET_NV_HW_CONFIG,
                CommandFunctionName = "SetHwConfig",
                Parameters = new List<parameterDefs>()
                {
                    new parameterDefs() {ParaText="Hardware configure value", NumberOfBytes=1}
                }
            },
            new commandClass
            {
                CommandText = "Set hardware configuration response",
                Display = false,
                CmdId = (byte)DVTMANCommands.RSP_SET_NV_HW_CONFIG,
            },
            new commandClass
            {
                CommandText = "Set pcba config in nv flash",
                Display = true,
                CmdId = (byte)DVTMANCommands.CMD_SET_NV_PCBA,
                CommandFunctionName = "SetPCBAData",
                Parameters = new List<parameterDefs>()
                {
                    new parameterDefs() { ParaText="PCBA PN", NumberOfBytes=11, ReturnValue=false},
                    new parameterDefs() { ParaText="PCBA SN", NumberOfBytes=15, ReturnValue=false},
                }
            },
            new commandClass{
                CommandText = "Set pcba config in nv flash response",
                Display = false,
                CmdId = (byte)DVTMANCommands.RSP_SET_NV_PCBA,
                 Parameters = new List<parameterDefs>()
                {
                    new parameterDefs() {ParaText="stats", NumberOfBytes = 1}
                }
           },
            new commandClass{
                CommandText = "Set hla config in nv flash",
                Display = true,
                CmdId = (byte)DVTMANCommands.CMD_SET_NV_HLA,
                CommandFunctionName = "SetHLAData",
                Parameters = new List<parameterDefs>()
                {
                    new parameterDefs() { ParaText="HLA PN", NumberOfBytes=11, ReturnValue=false},
                    new parameterDefs() { ParaText="HLA SN", NumberOfBytes=15, ReturnValue=false},
                    new parameterDefs() { ParaText="Model", NumberOfBytes=16, ReturnValue=false}
                }
            },
            new commandClass{
                CommandText = "Set hla config in nv flash response",
                Display = false,
                CmdId = (byte)DVTMANCommands.RSP_SET_NV_HLA,
                Parameters = new List<parameterDefs>()
                {
                    new parameterDefs() {ParaText="stats", NumberOfBytes = 1}
                }
            },
            new commandClass{
                CommandText = "Set temporary radio config",
                Display = true,
                CmdId = (byte)DVTMANCommands.CMD_SET_RADIO,
                CommandFunctionName = "SetRadioData",
                Parameters = new List<parameterDefs>()
                {
                    new parameterDefs() {ParaText="Channel ID", NumberOfBytes = 1, ReturnValue=false},
                    new parameterDefs() {ParaText="Pan ID", NumberOfBytes = 1, ReturnValue=false},
                    new parameterDefs() {ParaText="TX Power", NumberOfBytes = 1, ReturnValue=false},
                    new parameterDefs() {ParaText="Key", NumberOfBytes = 16, ReturnValue=false},
                    new parameterDefs() {ParaText="TLL", NumberOfBytes = 1, ReturnValue=false},
                    new parameterDefs() {ParaText="Rate", NumberOfBytes = 1, ReturnValue=false},
               }
            },
            new commandClass{
                CommandText = "Set temporary radio config response",
                Display = false,
                CmdId = (byte)DVTMANCommands.RSP_SET_RADIO,
            },
            new commandClass{
                CommandText = "Clear non-violatile config",
                Display = true,
                CmdId = (byte)DVTMANCommands.CMD_SET_NV_CLEAR,
                CommandFunctionName = "ClearManData"
            },
            new commandClass{
                CommandText = "Clear non-violatile config response",
                Display = false,
                CmdId = (byte)DVTMANCommands.RSP_SET_NV_CLEAR,
            },
            new commandClass{
                CommandText = "Return firmware, bootloader, Dvtman",
                Display = true,
                CmdId = (byte)DVTMANCommands.CMD_DISPLAY_VERSION,
                CommandFunctionName = "GetImageID",
                Parameters = new List<parameterDefs>()
                {
                    new parameterDefs() {ParaText="Image ID", NumberOfBytes=10, ReturnValue=true}
                }
            },
            new commandClass{
                CommandText = "Return firmware, bootloader,	Dvtman response",
                Display = false,
                CmdId = (byte)DVTMANCommands.RSP_DISPLAY_VERSION
            },
   //         new commandClass{
			//	CommandText = "Return current ble mode",
			//	Display = true,
			//	CmdId = (byte)DVTMANCommands.CMD_DISPLAY_BLE_MODE
			//},
   //         new commandClass{
			//	CommandText = "Return current ble mode response",
			//	Display = false,
			//	CmdId = (byte)DVTMANCommands.RSP_DISPLAY_BLE_MODE
			//},
            new commandClass{
                CommandText = "Return both radio & ble",
                Display = true,
                CmdId = (byte)DVTMANCommands.CMD_DISPLAY_NV_MAC,
                CommandFunctionName = "GetMACData",
                Parameters = new List<parameterDefs>()
                {
                    new parameterDefs() {ParaText="MAC1", NumberOfBytes=12, ReturnValue=true},
                    new parameterDefs() {ParaText="MAC2", NumberOfBytes=12, ReturnValue=true}
                }
            },
            new commandClass{
                CommandText = "Return both radio & ble response",
                Display = false,
                CmdId = (byte)DVTMANCommands.RSP_DISPLAY_NV_MAC
            },
            new commandClass{
                CommandText = "Return pcba config in nv flash",
                Display = true,
                CmdId = (byte)DVTMANCommands.CMD_DISPLAY_NV_PCBA,
                CommandFunctionName = "GetPCBAData",
                Parameters = new List<parameterDefs>()
                {
                    new parameterDefs() { ParaText="PCBA PN", NumberOfBytes=11, ReturnValue=true},
                    new parameterDefs() { ParaText="PCBA SN", NumberOfBytes=15, ReturnValue=true},
                }
            },
            new commandClass{
                CommandText = "Return pcba config in nv flash response",
                Display = false,
                CmdId = (byte)DVTMANCommands.RSP_DISPLAY_NV_PCBA
            },
            new commandClass{
                CommandText = "Return hla config in nv flash",
                Display = true,
                CmdId = (byte)DVTMANCommands.CMD_DISPLAY_NV_HLA,
                CommandFunctionName = "GetHLAData",
                Parameters = new List<parameterDefs>()
                {
                    new parameterDefs() { ParaText="HLA PN", NumberOfBytes=11, ReturnValue=true},
                    new parameterDefs() { ParaText="HLA SN", NumberOfBytes=15, ReturnValue=true},
                    new parameterDefs() { ParaText="Model", NumberOfBytes=16, ReturnValue=true}
                }
            },
            new commandClass{
                CommandText = "Return hla config in nv flash response",
                Display = false,
                CmdId = (byte)DVTMANCommands.RSP_DISPLAY_NV_HLA
            },
            new commandClass{
                CommandText = "Return hardware config",
                Display = true,
                CmdId = (byte)DVTMANCommands.CMD_DISPLAY_NV_HW_CONFIG,
                CommandFunctionName = "GetHwConfigData",
                Parameters = new List<parameterDefs>()
                {
                    new parameterDefs() {ParaText="Hardware Configuration", NumberOfBytes=17, ReturnValue=true}
                }
            },
            new commandClass{
                CommandText = "Return hardware config response",
                Display = false,
                CmdId = (byte)DVTMANCommands.RSP_DISPLAY_NV_HW_CONFIG
            },
            new commandClass{
                CommandText = "Return current radio param used",
                Display = true,
                CmdId = (byte)DVTMANCommands.CMD_DISPLAY_RADIO,
                CommandFunctionName = "GetRadioData",
                Parameters = new List<parameterDefs>()
                {
                    new parameterDefs() {ParaText="Channel", NumberOfBytes=4, ReturnValue=true },
                    new parameterDefs() {ParaText="Pan", NumberOfBytes=4, ReturnValue=true },
                    new parameterDefs() {ParaText="TxPower", NumberOfBytes=4, ReturnValue=true },
                    new parameterDefs() {ParaText="Key", NumberOfBytes=16, ReturnValue=true },
                    new parameterDefs() {ParaText="ttl", NumberOfBytes=4, ReturnValue=true },
                    new parameterDefs() {ParaText="datarate", NumberOfBytes=4, ReturnValue=true },
                }
            },
            new commandClass{
                CommandText = "Return current radio param used response",
                Display = false,
                CmdId = (byte)DVTMANCommands.RSP_DISPLAY_RADIO
            },
            new commandClass{
                CommandText = "power on self test",
                Display = true,
                CmdId = (byte)DVTMANCommands.CMD_PERFORM_POST
            },
            new commandClass{
                CommandText = "power on self tests response",
                Display = false,
                CmdId = (byte)DVTMANCommands.RSP_PERFORM_POST
            },
            new commandClass{
                CommandText = "change over to application image",
                Display = true,
                CmdId = (byte)DVTMANCommands.CMD_PERFORM_CHANGE,
                CommandFunctionName = "PreformChangeOver",
            },
            new commandClass{
                CommandText = "change over to application image response",
                Display = false,
                CmdId = (byte)DVTMANCommands.RSP_PERFORM_CHANGE,
                Parameters = new List<parameterDefs>()
                {
                    new parameterDefs() {ParaText="stats", NumberOfBytes = 1}
                }
            },
            new commandClass{
                CommandText = "temperature calibration. Input current room temperarture",
                Display = true,
                CmdId = (byte)DVTMANCommands.CMD_PERFORM_TEMP_CAL
            },
            new commandClass{
                CommandText = "temperature calibration response",
                Display = false,
                CmdId = (byte)DVTMANCommands.RSP_PERFORM_TEMP_CAL
            },
            new commandClass{
                CommandText = "specify dim1 voltage and dim2 voltage in range of 0 to 100(0~10v),read external measurement",
                Display = true,
                CmdId = (byte)DVTMANCommands.CMD_PERFORM_DIM_VOLTAGE,
                CommandFunctionName = "SetDimVoltage",
                Parameters = new List<parameterDefs>()
                {
                    new parameterDefs() {ParaText="Dim1(0.0 to 10.0", NumberOfBytes=4, ReturnValue=false},
                    new parameterDefs() {ParaText="Dim2(0.0 to 10.0", NumberOfBytes=4, ReturnValue=false},
                }
            },
            new commandClass{
                CommandText = "Dim response",
                Display = false,
                CmdId = (byte)DVTMANCommands.RSP_PERFORM_DIM_VOLTAGE,
                Parameters = new List<parameterDefs>()
                {
                    new parameterDefs() {ParaText="stats", NumberOfBytes = 1}
                }
            },
            new commandClass{
                CommandText = "Set LEDs",
                Display = true,
                CmdId = (byte)DVTMANCommands.CMD_PERFORM_LED,
                CommandFunctionName = "SetLeds",
                Parameters = new List<parameterDefs>()
                {
                    new parameterDefs() {ParaText="Red(ON/OFF)", NumberOfBytes = 3, ReturnValue = false},
                    new parameterDefs() {ParaText="Green(ON/OFF)", NumberOfBytes = 3, ReturnValue = false},
                    new parameterDefs() {ParaText="Blue(ON/OFF)", NumberOfBytes = 3, ReturnValue = false},
                }
            },
            new commandClass{
                CommandText = "Set LEDs response",
                Display = false,
                CmdId = (byte)DVTMANCommands.RSP_PERFORM_LED,
                Parameters = new List<parameterDefs>()
            },
            new commandClass{
                CommandText = "Return pir, ambient,	temperature",
                Display = true,
                CmdId = (byte)DVTMANCommands.CMD_PERFORM_SENSOR_READ,
                CommandFunctionName = "GetSensorData",
                Parameters = new List<parameterDefs>()
                {
                    new parameterDefs() {ParaText="PIR", NumberOfBytes=6, ReturnValue=true },
                    new parameterDefs() {ParaText="Ambient", NumberOfBytes=6, ReturnValue=true },
                    new parameterDefs() {ParaText="Temperature", NumberOfBytes=6, ReturnValue=true },
                }
            },
            new commandClass{
				CommandText = "Return pir, ambient, temperatur response",
				Display = false,
				CmdId = (byte)DVTMANCommands.RSP_PERFORM_SENSOR_READ
			},
            new commandClass{
				CommandText = "System reboot",
				Display = true,
				CmdId = (byte)DVTMANCommands.CMD_PERFORM_REBOOT,
                CommandFunctionName = "PerformReboot"
            },
            new commandClass{
				CommandText = "System reboot response",
				Display = false,
				CmdId = (byte)DVTMANCommands.RSP_PERFORM_REBOOT,
                Parameters = new List<parameterDefs>()
                {
                    new parameterDefs() {ParaText="stats", NumberOfBytes = 1}
                }

            },
            new commandClass{
				CommandText = "ble mode tests between two devices which can be programmed into idle, scan raw or beacon modes",
				Display = true,
				CmdId = (byte)DVTMANCommands.CMD_PERFORM_BLE_MODE
			},
            new commandClass{
				CommandText = "ble test response",
				Display = false,
				CmdId = (byte)DVTMANCommands.RSP_PERFORM_BLE_MODE,
                Parameters = new List<parameterDefs>()
                {
                    new parameterDefs() {ParaText="stats", NumberOfBytes = 1}
                }

            },
            new commandClass{
				CommandText = "radio 802.15.4 test",
				Display = true,
				CmdId = (byte)DVTMANCommands.CMD_PERFORM_WTEST,
                CommandFunctionName = "PerformWtest",
                Parameters = new List<parameterDefs>()
                {
                    new parameterDefs() {ParaText="Ref Radio Mac", NumberOfBytes=12, ReturnValue=false},
                    new parameterDefs() {ParaText="Wait timeout(ms)", NumberOfBytes=12, ReturnValue=false},
                    new parameterDefs() {ParaText="Destination LQI", NumberOfBytes=12, ReturnValue=true},
                    new parameterDefs() {ParaText="Source LQI", NumberOfBytes=12, ReturnValue=true},
                }
            },
            new commandClass{
				CommandText = "radio 802.15.4 tes response",
				Display = false,
				CmdId = (byte)DVTMANCommands.RSP_PERFORM_WTEST,
                Parameters = new List<parameterDefs>()
                {
                    new parameterDefs() {ParaText="stats", NumberOfBytes = 1}
                }

            },
            new commandClass
            {
                CommandText = "Display Flash",
                Display = false,
                CmdId = (byte)DVTMANCommands.RSP_PERFORM_WTEST,
                CommandFunctionName = "NoCodeYet",
                Parameters = new List<parameterDefs>()
                {
                    new parameterDefs() {ParaText="stats", NumberOfBytes = 1}
                }
            },
                        new commandClass
            {
                CommandText = "Display Flash response",
                Display = false,
                CmdId = (byte)DVTMANCommands.RSP_PERFORM_WTEST,
                Parameters = new List<parameterDefs>()
                {
                    new parameterDefs() {ParaText="stats", NumberOfBytes = 1}
                }
            },
            new commandClass
            {
                CommandText = "Set DALI Interface",
                Display = false,
                CmdId = (byte)DVTMANCommands.RSP_PERFORM_WTEST,
                CommandFunctionName = "NoCodeYet",
                Parameters = new List<parameterDefs>()
                {
                    new parameterDefs() {ParaText="stats", NumberOfBytes = 1}
                }
            },
            new commandClass
            {
                CommandText = "Display Flash response",
                Display = false,
                CmdId = (byte)DVTMANCommands.RSP_PERFORM_WTEST,
                Parameters = new List<parameterDefs>()
                {
                    new parameterDefs() {ParaText="stats", NumberOfBytes = 1}
                }
            },
            new commandClass
            {
                CommandText = "Preform DALI TX RX Test",
                Display = false,
                CmdId = (byte)DVTMANCommands.RSP_PERFORM_WTEST,
                CommandFunctionName = "NoCodeYet",
                Parameters = new List<parameterDefs>()
                {
                    new parameterDefs() {ParaText="stats", NumberOfBytes = 1}
                }
            },
            new commandClass
            {
                CommandText = "DALI TX RX Test response",
                Display = false,
                CmdId = (byte)DVTMANCommands.RSP_PERFORM_WTEST,
                 CommandFunctionName = "NoCodeYet",
               Parameters = new List<parameterDefs>()
                {
                    new parameterDefs() {ParaText="stats", NumberOfBytes = 1}
                }
            },
            new commandClass
            {
                CommandText = "Set FL state",
                Display = true,
                CmdId = (byte)DVTMANCommands.CMD_SET_FL_STATE,
                CommandFunctionName = "SetFLState",
                Parameters = new List<parameterDefs>()
                {
                    new parameterDefs() {ParaText="Set FL State(0 fixtured, 1 fixtureless", NumberOfBytes = 1}
                }
            },
            new commandClass
            {
                CommandText = "Set FL state response",
                Display = false,
                CmdId = (byte)DVTMANCommands.RSP_SET_FIXTURELESS_STATE,
                Parameters = new List<parameterDefs>()
                {
                    new parameterDefs() {ParaText="stats", NumberOfBytes = 1}
                }
            },
            new commandClass
            {
                CommandText = "Display FL state",
                Display = true,
                CmdId = (byte)DVTMANCommands.CMD_DISPLAY_FL_STATE,
                CommandFunctionName = "DisplayFLState",
                Parameters = new List<parameterDefs>()
                {
                    new parameterDefs() {ParaText="FL State", NumberOfBytes = 1, ReturnValue=true}
                }
            },
            new commandClass
            {
                CommandText = "Display FL state response",
                Display = false,
                CmdId = (byte)DVTMANCommands.RSP_DISPLAY_FL_STATE,
                CommandFunctionName = "DisplayFLStateResponse",
            },

        };



        //-----------------------------------
        // Housekeeping
        //----------------------------------
        public bool OpenComm()
        {
            if (PortName == string.Empty )
            {
                LastErrorCode = FixtureErrorCodes.MissingInstrumentOption;
                LastErrorMessage = "Com port is not defined";
                return false;
            }
            try
            {
                if (serial == null)
                    serial = new SerialPort();
                serial.PortName = PortName;
                serial.BaudRate = BaudRate;
                serial.Handshake = Handshake.None;
                serial.Parity = Parity.None;
                serial.ReadTimeout = 100;
                serial.StopBits = StopBits.One;
                serial.WriteTimeout = 100;
                serial.ReadBufferSize = 2096;
                serial.WriteBufferSize = 512;
                serial.DtrEnable = true;
                serial.ErrorReceived += HandleError;
                bufferSaveIndex = 0;
                packetList.Clear();
                serial.Open();           // open the serial port
                Thread.Sleep(100);
            }
            catch (Exception ex)
            {
                LastErrorMessage = "Error opening port; " + ex.Message;
                LastErrorCode = FixtureErrorCodes.DUTConsoleOpenError;
                return false;
            }
            return true;
        }


        public bool OpenComm(string comport, int comspeed)
        {
            PortName = comport;
            BaudRate = comspeed;
            packetRxSeqNumber = 0;      // values for the transport layer of dut commications
            packetTxSeqNumber = 0;
            return OpenComm();
        }

        public bool IsOpen()
        {
            return serial.IsOpen;
        }

        public bool CloseComm()
        {
            try
            {
                if (serial != null)
                {
                    Thread.Sleep(500);          // just in case something else is in the process of closing it
                    if (serial.IsOpen)
                    {
                        serial.BaseStream.Flush();
                        serial.BaseStream.Close();
                        serial.Close();
                        serial.Dispose();
                        Thread.Sleep(100);
                    }
                }
            }
            catch (Exception ex)
            {
                LastErrorMessage = "Error while closing sensor console port. IsOpen = " + serial.IsOpen + "\n" + ex.Message;
            }
            return true;
        }

        public void SetDumpFileName(string directory, string baseTitle)
        {
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory);

            dumpFileName = directory + @"/" + baseTitle;
        }

        public void EndDump()
        {
            dumpFileName = string.Empty;
        }

        public void ClearBuffers()
        {
            if (serial.IsOpen)
            {
                packetList.Clear();
                serial.DiscardInBuffer();
                bufferSaveIndex = 0;
                Thread.Sleep(100);
            }
        }

        //-----------------------------------
        //  Commands
        //-----------------------------------

        public bool NoCodeYet()
        {
            LastErrorCode = DUTErrorCodes.dutCmdInputDataFormatError;
            LastErrorMessage = "Radio MAC must be 12 characters.";
            return false;
        }

        //----------------------
        //  Set commands


        /// <summary>
        /// Set the radio and ble MACS. The strings should have no colons.
        /// </summary>
        /// <remarks>
        /// P1: 6 bytes, enlighted radio MAC
        /// P2: 6 bytes, ble radio MAC
        /// ex: 6854f5112233 -> byte[0]=68, byte[1]=54, ....
        /// 
        /// LastErrorCode
        ///  2  ErrorCodes.Program.DUTComError
        ///  16  ErrorCodes.Program.CommandTimeout
        ///  19  ErrorCodes.Program.CmdResponsePayloadLenWrong
        ///  23  ErrorCodes.Program.CmdPacketFormatError
        ///  22  ErrorCodes.Program.CmdResponseHeaderLenWrong
        ///  21  ErrorCodes.Program.CmdMsgTypeNotDVTMAN
        ///  20  ErrorCodes.Program.CmdPacketCheckSumError
        /// </remarks>
        /// <param name="radioMAC">MAC for the enlighted radio. no colons</param>
        /// <param name="bleMAC">MAC for the ble radio. no colons</param>
        /// <returns></returns>
        public bool SetMACData(string radioMAC, string bleMAC)
        {
            bool status = true;
            byte[] cmd = new byte[14];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;


            // verify that data good
            if (radioMAC.Length != 12)
            {
                LastErrorCode = DUTErrorCodes.dutCmdInputDataFormatError;
                LastErrorMessage = "Radio MAC must be 12 characters.";
                return false;
            }
            if (bleMAC.Length != 12)
            {
                LastErrorCode = DUTErrorCodes.dutCmdInputDataFormatError;
                LastErrorMessage = "BLE MAC must be 12 characters.";
                return false;
            }
            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_SET_NV_MAC;
            for (int i = 0; i < 6; i++)
            {
                cmd[2 + i] = (byte) Int32.Parse(radioMAC.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
            }
            for (int i = 0; i < 6; i++)
            {
                cmd[8 + i] = (byte) Int32.Parse(bleMAC.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
            }

            // send it
            status = send_cmd(cmd);

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_SET_NV_MAC)
                    {
                        LastErrorCode = DUTErrorCodes.dutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_SET_NV_MAC].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = DUTErrorCodes.dutCmdResponseStatusFailed;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_SET_NV_MAC].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                }
            }
            else
            {
                // last error codes and message set by send_cmd
                return false;
            }
            // return status

            return true;
        }

        /// <summary>
        /// Set the hardware configuration data.
        /// </summary>
        /// <param name="ambientScaling"></param>
        /// <param name="pirLensType"></param>
        /// <remarks>
        /// LastErrorCode
        ///  2  ErrorCodes.Program.DUTComError
        ///  16  ErrorCodes.Program.CommandTimeout
        ///  19  ErrorCodes.Program.CmdResponsePayloadLenWrong
        ///  23  ErrorCodes.Program.CmdPacketFormatError
        ///  22  ErrorCodes.Program.CmdResponseHeaderLenWrong
        ///  21  ErrorCodes.Program.CmdMsgTypeNotDVTMAN
        ///  20  ErrorCodes.Program.CmdPacketCheckSumError
        /// </remarks>
        /// <returns></returns>
        public bool SetHwConfig(string hwConfigStr)
        {
            bool status = true;
            byte[] cmd;
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;
            byte hwConfig;

            if (!byte.TryParse(hwConfigStr, out hwConfig))
            {
                LastErrorCode = DUTErrorCodes.dutCmdInputDataFormatError;
                LastErrorMessage = Commmands[(int)DVTMANCommands.CMD_SET_NV_HW_CONFIG].CommandText + ". Value must be a byte.";
                return false;
            }

            // pakage the command
            cmd = new byte[3];
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_SET_NV_HW_CONFIG;
            cmd[2] = hwConfig;

            // send it
            status = send_cmd(cmd);

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_SET_NV_HW_CONFIG)
                    {
                        LastErrorCode = DUTErrorCodes.dutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_SET_NV_HW_CONFIG].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = DUTErrorCodes.dutCmdResponseStatusFailed;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_SET_NV_HW_CONFIG].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                }
                else                // timed out
                {
                    LastErrorCode = DUTErrorCodes.dutCommandTimeout;
                    LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_SET_NV_HW_CONFIG].CommandText + ". Timed out";
                    return false;

                }
            }
            else
            {
                // last error codes and message set by send_cmd
                return false;
            }
            // return status

            return true;
        }

        /// <summary>
        /// Set up the PCBA data
        /// </summary>
        /// <remarks>
        /// P1: partNo[15B]
        /// P2: serialNo[20B]
        /// 
        /// LastErrorCode
        ///  2   ErrorCodes.Program.DUTComError
        ///  16  ErrorCodes.Program.CommandTimeout
        ///  19  ErrorCodes.Program.CmdResponsePayloadLenWrong
        ///  23  ErrorCodes.Program.CmdPacketFormatError
        ///  22  ErrorCodes.Program.CmdResponseHeaderLenWrong
        ///  21  ErrorCodes.Program.CmdMsgTypeNotDVTMAN
        ///  20  ErrorCodes.Program.CmdPacketCheckSumError
        ///  
        /// </remarks>
        /// <param name="partNumber">string up to 15 bytes long</param>
        /// <param name="serialNumber">string up to 20 bytes long</param>
        /// <returns></returns>
        public bool SetPCBAData(string partNumber, string serialNumber)
        {
            bool status = true;
            byte[] cmd = new byte[2 + 15 + 20];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;
            byte[] precmd = new byte[2];

            // pakage the command
            precmd[0] = DVTMANMessageType;
            precmd[1] = (byte)DVTMANCommands.CMD_SET_NV_PCBA;
            byte[] pnarray = Encoding.ASCII.GetBytes(partNumber);
            byte[] snarray = Encoding.ASCII.GetBytes(serialNumber);
            Array.Copy(precmd, cmd, 2);
            Array.Copy(pnarray, 0, cmd, 2, pnarray.Length);
            Array.Copy(snarray, 0, cmd, 17, snarray.Length);

            // send it
            status = send_cmd(cmd);

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_SET_NV_PCBA)
                    {
                        LastErrorCode = DUTErrorCodes.dutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_SET_NV_PCBA].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = DUTErrorCodes.dutCmdResponseStatusFailed;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_SET_NV_PCBA].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                }
                else                // recive error. set by receive_response
                {
                    return false;

                }
            }
            else
            {
                // last error codes and message set by send_cmd
                return false;
            }
            // return status

            return true;
        }

        /// <summary>
        /// Set HLA data.
        /// </summary>
        /// <remarks>
        /// P1: hlaPartNo[15B, char]
        /// P2: hlsSerialNo [20B, char]
        /// P3: hlaModelname [12B, char]
        /// 
        /// </remarks>
        /// <param name="partno"></param>
        /// <param name="serialno"></param>
        /// <param name="modelname"></param>
        /// <returns></returns>
        public bool SetHLAData(string partNumber, string serialNumber, string modelname)
        {
            bool status = true;
            byte[] cmd = new byte[2 + 15 + 20 + 12];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;
            byte[] precmd = new byte[2];

            // pakage the command
            precmd[0] = DVTMANMessageType;
            precmd[1] = (byte)DVTMANCommands.CMD_SET_NV_HLA;
            byte[] pnarray = Encoding.ASCII.GetBytes(partNumber);
            byte[] snarray = Encoding.ASCII.GetBytes(serialNumber);
            byte[] modelarray = Encoding.ASCII.GetBytes(modelname);
            Array.Copy(precmd, cmd, 2);
            Array.Copy(pnarray, 0, cmd, 2, pnarray.Length);
            Array.Copy(snarray, 0, cmd, 17, snarray.Length);
            Array.Copy(modelarray, 0, cmd, 37, modelarray.Length);

            // send it
            status = send_cmd(cmd);

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_SET_NV_HLA)
                    {
                        LastErrorCode = DUTErrorCodes.dutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_SET_NV_HLA].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = DUTErrorCodes.dutCmdResponseStatusFailed;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_SET_NV_HLA].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                }
            }
            else
            {
                // last error codes and message set by send_cmd
                return false;
            }
            // return status

            return true;
        }

        public bool SetRadioData(string channelID, string panID, string txpower, string key, string ttl, string rate)
        {
            bool status = true;
            byte[] cmd = new byte[2 + 22];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;
            uint panIDuint;
            byte[] rawKey;
            string correctedKey = key;

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_SET_RADIO;
            if (!byte.TryParse(channelID, out cmd[2]))
            {
                LastErrorCode = DUTErrorCodes.dutCmdInputDataFormatError;
                LastErrorMessage = "Channel ID needs to be a byte value.";
                return false;
            }
            if (!uint.TryParse(panID, out panIDuint))
            {
                LastErrorCode = DUTErrorCodes.dutCmdInputDataFormatError;
                LastErrorMessage = "Pan ID needs to be a unsigned int value.";
                return false;
            }
            cmd[3] = (byte)(panIDuint / 256);
            cmd[4] = (byte)(panIDuint - ((panIDuint / 256) * 256));
            if (!byte.TryParse(txpower, out cmd[5]))
            {
                LastErrorCode = DUTErrorCodes.dutCmdInputDataFormatError;
                LastErrorMessage = "TX Power needs to be a byte value.";
                return false;
            }
            if (key.Length > 16)
                correctedKey = key.Substring(0, 16);
            if (key.Length < 16)
                correctedKey = key.PadRight(16, '\0');
            rawKey = Encoding.ASCII.GetBytes(correctedKey);
            Array.Copy(rawKey, 0, cmd, 6, 16);
            if (!byte.TryParse(ttl, out cmd[22]))
            {
                LastErrorCode = DUTErrorCodes.dutCmdInputDataFormatError;
                LastErrorMessage = "TTL needs to be a byte value.";
                return false;
            }
            if (!byte.TryParse(rate, out cmd[23]))
            {
                LastErrorCode = DUTErrorCodes.dutCmdInputDataFormatError;
                LastErrorMessage = "Rate needs to be a byte value.";
                return false;
            }
            // send it
            ClearBuffers();
            status = send_cmd(cmd);


            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_SET_RADIO)
                    {
                        LastErrorCode = DUTErrorCodes.dutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_SET_RADIO].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = DUTErrorCodes.dutCmdResponseStatusFailed;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_SET_RADIO].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                }
                else            // issue geting the response
                {
                    // last error codes and message set by send_cmd
                    return false;
                }
            }
            else
            {
                // last error codes and message set by send_cmd
                return false;
            }
            // return status

            return true;
        }

        public bool PreformTempCal(uint roomtemp, ref uint rawtemp)
        {
            bool status = true;
            byte[] cmd = new byte[3];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_PERFORM_TEMP_CAL;
            cmd[2] = (byte)roomtemp;
            // send it
            status = send_cmd(cmd);

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_PERFORM_TEMP_CAL)
                    {
                        LastErrorCode = DUTErrorCodes.dutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending Temp Cal. Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = DUTErrorCodes.dutCmdResponseStatusFailed;
                        LastErrorMessage = "Error sending Temp Cal. Returned cmd: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                    else
                    {
                        rawtemp = (uint)payload[0] + ((uint)payload[1] * 256);
                    }
                }
            }
            else
            {
                // last error codes and message set by send_cmd
                return false;
            }
            // return status

            return true;
        }

        /// <summary>
        /// Set the two diming outputs
        /// </summary>
        /// <remarks>
        /// P1: dim1 voltage[1B] 0-100 -> 0-10V
        /// P2: dim2 voltage[1B] 0-100 -> 0-10V
        /// 
        /// LastErrorCode
        ///  2  ErrorCodes.Program.DUTComError
        ///  3  ErrorCodes.Program.dutUnexpectedCmdReturn
        ///  16  ErrorCodes.Program.CommandTimeout
        ///  19  ErrorCodes.Program.CmdResponsePayloadLenWrong
        ///  23  ErrorCodes.Program.CmdPacketFormatError
        ///  22  ErrorCodes.Program.CmdResponseHeaderLenWrong
        ///  21  ErrorCodes.Program.CmdMsgTypeNotDVTMAN
        ///  20  ErrorCodes.Program.CmdPacketCheckSumError
        /// </remarks>
        /// <param name="dim1"></param>
        /// <param name="dim2"></param>
        /// <returns></returns>
        public bool SetDimVoltage(string dim1, string dim2)
        {
            bool status = true;
            byte[] cmd = new byte[2 + 2];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;

            byte bdim1;
            byte bdim2;
            double ddim1;
            double ddim2;
            if (!double.TryParse(dim1, out ddim1))
            {
                LastErrorCode = DUTErrorCodes.dutCmdInputDataFormatError;
                LastErrorMessage = "Dim1 must be a double value.";
                return false;
            }
            if (ddim1 > 10.0)
            {
                LastErrorCode = DUTErrorCodes.dutCmdInputDataFormatError;
                LastErrorMessage = "Dim1 must be 0.0 to 10.0 value.";
                return false;
            }
            bdim1 = (byte)(ddim1 * 10);
            if (!double.TryParse(dim2, out ddim2))
            {
                LastErrorCode = DUTErrorCodes.dutCmdInputDataFormatError;
                LastErrorMessage = "Dim2 must be a double value.";
                return false;
            }
            if (ddim2 > 10.0)
            {
                LastErrorCode = DUTErrorCodes.dutCmdInputDataFormatError;
                LastErrorMessage = "Dim2 must be 0.0 to 10.0 value.";
                return false;
            }
            bdim2 = (byte)(ddim2 * 10);

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_PERFORM_DIM_VOLTAGE;
            cmd[2] = (byte)bdim1;
            cmd[3] = (byte)bdim2;

            // send it
            status = send_cmd(cmd);


            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_PERFORM_DIM_VOLTAGE)
                    {
                        LastErrorCode = DUTErrorCodes.dutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_PERFORM_DIM_VOLTAGE].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = DUTErrorCodes.dutCmdResponseStatusFailed;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_PERFORM_DIM_VOLTAGE].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                }
                else            // issue geting the response
                {
                    // last error codes and message set by send_cmd
                    return false;
                }
            }
            else
            {
                // last error codes and message set by send_cmd
                return false;
            }
            // return status

            return true;
        }

        public bool SetLeds(string redon, string greenon, string blueon)
        {
            bool status = true;
            byte[] cmd = new byte[2 + 3];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_PERFORM_LED;
            cmd[2] = (byte)((redon.ToUpper() == "ON")? 1:0);
            cmd[3] = (byte)((greenon.ToUpper() == "ON") ? 1 : 0);
            cmd[4] = (byte)((blueon.ToUpper() == "ON") ? 1 : 0);

            // send it
            status = send_cmd(cmd);


            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_PERFORM_LED)
                    {
                        LastErrorCode = DUTErrorCodes.dutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_PERFORM_LED].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = DUTErrorCodes.dutCmdResponseStatusFailed;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_PERFORM_LED].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                }
                else            // issue geting the response
                {
                    // last error codes and message set by send_cmd
                    return false;
                }
            }
            else
            {
                // last error codes and message set by send_cmd
                return false;
            }
            // return status

            return true;
        }

        //--------------------------------------------
        //  query commands
        //--------------------------------------------
        public bool GetMsgVersion(ref int version)
        {
            bool status;
            byte[] cmd = new byte[2];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;
            byte[] precmd = new byte[2];

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_DISPLAY_MSG_VERSION;

            // send it
            status = send_cmd(cmd);

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_DISPLAY_MSG_VERSION)
                    {
                        LastErrorCode = DUTErrorCodes.dutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_MSG_VERSION].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = DUTErrorCodes.dutCmdResponseStatusFailed;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_MSG_VERSION].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                    version = payload[0];
                }
            }
            else
            {
                // last error codes and message set by send_cmd
                return false;
            }
            // return status

            return true;
        }

        /// <summary>
        /// Wait for the Message Version packet
        /// </summary>
        /// <remarks>
        /// LastErrorCode
        /// 3  -  ErrorCodes.Program.dutUnexpectedCmdReturn
        ///  16  ErrorCodes.Program.CommandTimeout, waiting for response
        ///  19  ErrorCodes.Program.CmdResponsePayloadLenWrong
        ///  23  ErrorCodes.Program.CmdPacketFormatError
        ///  22  ErrorCodes.Program.CmdResponseHeaderLenWrong
        ///  21  ErrorCodes.Program.CmdMsgTypeNotDVTMAN
        ///  20  ErrorCodes.Program.CmdPacketCheckSumError
        /// </remarks>
        /// <param name="version"></param>
        /// <param name="timeoutms"></param>
        /// <returns></returns>
        public bool WaitForMsgVersion(ref int version, int timeoutms)
        {
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;

            LastErrorCode = 0;
            LastErrorMessage = string.Empty;


            // look for response
            if (receive_response(ref cmdId, ref cmdStatus, out payload, timeoutms))
            {
                if (cmdId != (byte)DVTMANCommands.RSP_DISPLAY_MSG_VERSION)
                {
                    LastErrorCode = DUTErrorCodes.dutUnexpectedCmdReturn;
                    LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_MSG_VERSION].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                    return false;
                }
                if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                {
                    LastErrorCode = DUTErrorCodes.dutCmdResponseStatusFailed;
                    LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_MSG_VERSION].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                    return false;
                }
                version = payload[0];
            }
            else
            {
                return false;       // receive_response sets error messages
            }

            return true;
        }

        /// <summary>
        /// Get the firmware versions
        /// </summary>
        /// <remarks>
        /// P1: stats
        /// P2: image ID[4B LSB]
        /// LastErrorCode
        ///  3  ErrorCodes.Program.dutUnexpectedCmdReturn
        ///  2  ErrorCodes.Program.DUTComError
        ///  18 ErrorCodes.Program.CmdResponseStatusFailed
        /// </remarks>
        /// <param name="imageID></param>
        /// <returns></returns>
        public bool GetImageID(out string imageID)
        {
            bool status;
            byte[] cmd = new byte[2];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;
            byte[] precmd = new byte[2];

            imageID = "0";      // default if not found or error

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_DISPLAY_VERSION;

            ClearBuffers();
            // send it
            status = send_cmd(cmd);

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_DISPLAY_VERSION)
                    {
                        LastErrorCode = DUTErrorCodes.dutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_VERSION].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = DUTErrorCodes.dutCmdResponseStatusFailed;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_VERSION].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                    string lclimageID = BitConverter.ToString(payload);
                    imageID = lclimageID.Replace("-", "");                  // strip out '-'s
                }
            }
            else
            {
                // last error codes and message set by send_cmd
                return false;
            }
            // return status

            return true;
        }

        public bool GetBLEMode(ref uint mode)
        {
            return false;
        }

        /// <summary>
        /// read the mac data. will only return the first two.
        /// </summary>
        /// <param name="mac"></param>
        /// <param name="mac2"></param>
        /// <returns></returns>
        public bool GetMACData(out string mac, out string mac2)
        {
            string mac3 = string.Empty;
            return GetMACData3(out mac, out mac2, out mac3);
        }

        /// <summary>
        /// read the mac data.
        /// </summary>
        /// <remarks>
        /// P1: stats
        /// P2: radio mac[6B]
        /// P3: ble mac[6B]
        /// P4: ethernet mac[6B] - only when unit is gateway
        /// </remarks>
        /// <param name="mac"></param>
        /// <param name="mac2"></param>
        /// <param name="mac3"></param>
        /// <returns></returns>
        public bool GetMACData3(out string mac, out string mac2, out string mac3)
        {
            bool status;
            byte[] cmd = new byte[2];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;
            byte[] precmd = new byte[2];
            string lclmac = string.Empty;
            string lclmac2 = string.Empty;
            string lclmac3 = string.Empty;

            mac = string.Empty;
            mac2 = string.Empty;
            mac3 = string.Empty;

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_DISPLAY_NV_MAC;

            // send it
            status = send_cmd(cmd);

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload, 3000))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_DISPLAY_NV_MAC)
                    {
                        LastErrorCode = DUTErrorCodes.dutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_NV_MAC].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = DUTErrorCodes.dutCmdResponseStatusFailed;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_NV_MAC].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                    byte[] macarray = new byte[6];
                    byte[] mac2array = new byte[6];
                    byte[] mac3array = new byte[6];
                    Array.Copy(payload, 0, macarray, 0, 6);
                    Array.Copy(payload, 6, mac2array, 0, 6);
                    lclmac = BitConverter.ToString(macarray);
                    lclmac2 = BitConverter.ToString(mac2array);
                    if (payload.Length > 12)    // if there is a third mac
                    {
                        Array.Copy(payload, 12, mac3array, 0, 6);
                        lclmac3 = BitConverter.ToString(mac3array);
                    }
                    mac = lclmac.Replace("-", "");                  // strip out '-'s
                    mac2 = lclmac2.Replace("-", "");
                    mac3 = lclmac3.Replace("-", "");
                }
                else
                {
                    return false;
                }
            }
            else
            {
                // last error codes and message set by send_cmd
                return false;
            }
            // return status

            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// LastErrorCode
        ///  2  ErrorCodes.Program.DUTComError
        ///  3  ErrorCodes.Program.dutUnexpectedCmdReturn
        ///  16  ErrorCodes.Program.CommandTimeout
        ///  19  ErrorCodes.Program.CmdResponsePayloadLenWrong
        ///  23  ErrorCodes.Program.CmdPacketFormatError
        ///  22  ErrorCodes.Program.CmdResponseHeaderLenWrong
        ///  21  ErrorCodes.Program.CmdMsgTypeNotDVTMAN
        ///  20  ErrorCodes.Program.CmdPacketCheckSumError
        /// </remarks>
        /// <param name="partno"></param>
        /// <param name="serialno"></param>
        /// <returns></returns>
        public bool GetPCBAData(out string partno, out string serialno)
        {
            bool status;
            byte[] cmd = new byte[2];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;
            byte[] precmd = new byte[2];

            partno = string.Empty;
            serialno = string.Empty;

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_DISPLAY_NV_PCBA;

            // send it
            status = send_cmd(cmd);

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload, 3000))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_DISPLAY_NV_PCBA)
                    {
                        LastErrorCode = DUTErrorCodes.dutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_NV_PCBA].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = DUTErrorCodes.dutCmdResponseStatusFailed;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_NV_PCBA].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                    byte[] pnarray = new byte[15];
                    byte[] snarray = new byte[20];
                    Array.Copy(payload, 0, pnarray, 0, 15);
                    Array.Copy(payload, 15, snarray, 0, 20);
                    partno = string.Empty;
                    if (pnarray[0] != 0xFF)     // if it has valid data
                        partno = Encoding.ASCII.GetString(pnarray).TrimEnd('\0');
                    serialno = string.Empty;
                    if (snarray[0] != 0xFF)     // if it has valid data
                        serialno = Encoding.ASCII.GetString(snarray).TrimEnd('\0');
                }
            }
            else
            {
                // last error codes and message set by send_cmd
                return false;
            }
            // return status

            return true;
        }

        /// <summary>
        /// Get the HLA data.
        /// </summary>
        /// <remarks>
        /// P1: stats
        /// P2: hlaPartNo[15B, char]
        /// P3: hlaSerialNo[20B, char]
        /// P4: hlaModelname[12B, hex]
        /// </remarks>
        /// <param name="partno"></param>
        /// <param name="serialno"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool GetHLAData(ref string partno, ref string serialno, ref string model)
        {
            bool status;
            byte[] cmd = new byte[2];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;
            byte[] precmd = new byte[2];

            // make sure there is no old data
            partno = string.Empty;
            serialno = string.Empty;
            model = string.Empty;

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_DISPLAY_NV_HLA;

            // send it
            status = send_cmd(cmd);

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload, 3000))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_DISPLAY_NV_HLA)
                    {
                        LastErrorCode = DUTErrorCodes.dutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_NV_HLA].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = DUTErrorCodes.dutCmdResponseStatusFailed;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_NV_HLA].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                    byte[] pnarray = new byte[15];
                    byte[] snarray = new byte[20];
                    byte[] modelarray = new byte[12];
                    Array.Copy(payload, 0, pnarray, 0, 15);
                    Array.Copy(payload, 15, snarray, 0, 20);
                    Array.Copy(payload, 35, modelarray, 0, 12);
                    partno = string.Empty;
                    if (pnarray[0] != 0xFF)     // if it has real data
                        partno = Encoding.ASCII.GetString(pnarray).TrimEnd('\0');
                    serialno = string.Empty;
                    if (snarray[0] != 0xFF)     // if it has real data
                        serialno = Encoding.ASCII.GetString(snarray).TrimEnd('\0');
                    model = string.Empty;
                    if (modelarray[0] != 0xFF)     // if it has real data
                        model = Encoding.ASCII.GetString(modelarray).TrimEnd('\0');
                }
                else
                    return false;       // error reciving response
            }
            else
            {
                // last error codes and message set by send_cmd
                return false;
            }
            // return status

            return true;
        }

        /// <summary>
        /// Return the hardware config. 
        /// </summary>
        /// <remarks>
        /// returns:
        ///     P1:stats
        ///     P2:pirSensorType[1B] - this is the byte that is programed with CMD_SET_NV_HW_CONF
        ///     P3 - P17: other stuff
        ///     
        /// LastErrorCode
        ///  2  ErrorCodes.Program.DUTComError
        ///  16  ErrorCodes.Program.CommandTimeout
        ///  19  ErrorCodes.Program.CmdResponsePayloadLenWrong
        ///  23  ErrorCodes.Program.CmdPacketFormatError
        ///  22  ErrorCodes.Program.CmdResponseHeaderLenWrong
        ///  21  ErrorCodes.Program.CmdMsgTypeNotDVTMAN
        ///  20  ErrorCodes.Program.CmdPacketCheckSumError
        /// </remarks>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool GetHwConfigData(out string data)
        {
            bool status = true;
            byte[] cmd = new byte[2];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_DISPLAY_NV_HW_CONFIG;

            // send it
            status = send_cmd(cmd);         // sets LastErrorCode and LastErrorMessage if error

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload))
                {
                    if (payload.Length != 0)
                    {
                        data = BitConverter.ToString(payload);
                    }
                    else
                        data = string.Empty;

                    if (cmdId != (byte)DVTMANCommands.RSP_DISPLAY_NV_HW_CONFIG)
                    {
                        LastErrorCode = DUTErrorCodes.dutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_NV_HW_CONFIG].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = DUTErrorCodes.dutCmdResponseStatusFailed;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_NV_HW_CONFIG].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                }
                else            // timed out
                {
                    data = string.Empty;
                    return false;

                }
            }
            else
                data = string.Empty;
            return status;
        }

        /// <summary>
        /// Get the radio data.
        /// </summary>
        /// <remarks>
        /// P1: stats
        /// P2: channelID[1B]
        /// P3: panID[2B]
        /// P4: txPower[1B]
        /// P5: encrypt_key[16B]
        /// P6: ttl[1B]
        /// P7: dataRate[1B]
        /// </remarks>
        /// <param name="channel"></param>
        /// <param name="pan"></param>
        /// <param name="txpower"></param>
        /// <param name="key"></param>
        /// <param name="ttl"></param>
        /// <param name="datarate"></param>
        /// <returns></returns>
        public bool GetRadioData(out string channel, out string pan, out string txpower, out string key, out string ttl, out string datarate)
        {
            bool status;
            byte[] cmd = new byte[2];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;
            byte[] precmd = new byte[2];
            byte[] keyarray = new byte[16];

            channel = string.Empty;
            pan = string.Empty;
            txpower = string.Empty;
            key = string.Empty;
            ttl = string.Empty;
            datarate = string.Empty;

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_DISPLAY_RADIO;

            // send it
            status = send_cmd(cmd);         // sets LastErrorCode and LastErrorMessage if error

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_DISPLAY_RADIO)
                    {
                        LastErrorCode = DUTErrorCodes.dutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_RADIO].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = DUTErrorCodes.dutCmdResponseStatusFailed;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_DISPLAY_RADIO].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                }
                else            // timed out
                {
                    return false;

                }
                channel = payload[0].ToString();
                pan = ((payload[1] * 256) + payload[2]).ToString("X4");
                txpower = payload[3].ToString();
                Array.Copy(payload, 4, keyarray, 0, 16);
                key = Encoding.ASCII.GetString(keyarray).TrimEnd('\0');
                ttl = payload[20].ToString();
                datarate = payload[21].ToString();
            }
        return status;
        }

        public bool GetSensorData(out string pir, out string ambient, out string temp)
        {
            bool status;
            byte[] cmd = new byte[2];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;
            byte[] precmd = new byte[2];

            pir = "no return";
            ambient = "no return";
            temp = "no return";

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_PERFORM_SENSOR_READ;

            // send it
            status = send_cmd(cmd);         // sets LastErrorCode and LastErrorMessage if error

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload, 2000))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_PERFORM_SENSOR_READ)
                    {
                        LastErrorCode = DUTErrorCodes.dutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_PERFORM_SENSOR_READ].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = DUTErrorCodes.dutCmdResponseStatusFailed;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_PERFORM_SENSOR_READ].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                }
                else            // recive error
                {
                    return false;

                }
                pir = (payload[0] + (payload[1] * 256)).ToString();
                ambient = (payload[2] + (payload[3] * 256) + (payload[4] * 65536) + (payload[5] * 16777216)).ToString();
                temp = (payload[6] + (payload[7] * 256)).ToString();
            }
            return status;
        }

        //----------------------------------------------
        //  action commands
        //---------------------------------------------
        /// <summary>
        /// 
        /// </summary>
        /// <remarks>
        /// LastErrorCode
        ///  3  ErrorCodes.Program.dutUnexpectedCmdReturn
        ///  16 ErrorCodes.Program.CommandTimeout
        /// </remarks>
        /// <returns></returns>
        public bool ClearManData()
        {
            bool status;
            byte[] cmd = new byte[2];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;
            byte[] precmd = new byte[2];

            ClearBuffers();             // make sure buffers are cleared. Could be 1st command of test

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_SET_NV_CLEAR;

            // send it
            status = send_cmd(cmd);

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_SET_NV_CLEAR)
                    {
                        LastErrorCode = DUTErrorCodes.dutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_SET_NV_CLEAR].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = DUTErrorCodes.dutCmdResponseStatusFailed;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_SET_NV_CLEAR].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                    Thread.Sleep(500);          // seeing if this helps the 'port closed' issue with the reset command issued after this
                }
                else            // timed out
                {
                    return false;

                }
            }
            return true;
        }

        /// <summary>
        /// Reads the POST return code.
        /// </summary>
        /// <remarks>
        /// P1: stats P2: crc saved[4B LSB] P3: crc calculated[4B LSB]
        /// </remarks>
        /// <param name="statuscode"></param>
        /// <returns>false = error sending or reciveing</returns>
        public bool PerformPost(ref int statuscode, ref UInt32 savedCRC, ref UInt32 calCRC)
        {
            byte[] cmd = new byte[2];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;
            byte[] precmd = new byte[2];

            savedCRC = 0;
            calCRC = 0;

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_PERFORM_POST;

            // verify response
            if (send_cmd(cmd))
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_PERFORM_POST)
                    {
                        LastErrorCode = DUTErrorCodes.dutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_PERFORM_POST].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    savedCRC = (uint)payload[0] + ((uint)payload[1] * 256) + ((uint)payload[2] * 65536) + ((uint)payload[3] * 16777216);
                    calCRC = (uint)payload[4] + ((uint)payload[5] * 256) + ((uint)payload[6] * 65536) + ((uint)payload[7] * 16777216);
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = DUTErrorCodes.dutCmdResponseStatusFailed;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_PERFORM_POST].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                }
                else            // did not recieve response to command
                {
                    return false;
                }
            }
            else            // failed to send the command. LastErrorCode and LastErrorMessage set by send_cmd
            {
                return false;

            }

            statuscode = (int)cmdStatus;
            return true;
        }

 //       public bool PreformChangeOver(int timeoutMs, ref int statuscode)
        public bool PreformChangeOver()
        {
            byte[] cmd = new byte[2];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;
            byte[] precmd = new byte[2];
            int timeoutMs = 2000;



            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_PERFORM_CHANGE;

            ClearBuffers();

            // verify response
            if (send_cmd(cmd))
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload, timeoutMs))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_PERFORM_CHANGE)
                    {
                        LastErrorCode = DUTErrorCodes.dutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_PERFORM_CHANGE].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = DUTErrorCodes.dutCmdResponseStatusFailed;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_PERFORM_CHANGE].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                }
                else        // timed out
                {
                    return false;

                }
            }
            else            // failed to send the command. LastErrorCode and LastErrorMessage set by send_cmd
            {
                return false;

            }

 //           statuscode = (int)cmdStatus;
            return true;
        }

        /// <summary>
        /// Send Reboot command to unit.
        /// Send parameters: none
        /// Response parameters: P1: status
        /// </summary>
        /// <returns></returns>
        public bool PerformReboot()
        {
            bool status = true;
            byte[] cmd = new byte[2];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_PERFORM_REBOOT;
            // send it
            status = send_cmd(cmd);

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_PERFORM_REBOOT)
                    {
                        LastErrorCode = DUTErrorCodes.dutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending reboot. Returned cmd: " + cmdId.ToString("X2");
                        return false; 
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = DUTErrorCodes.dutCmdResponseStatusFailed;
                        LastErrorMessage = "Error sending reboot. Returned cmd: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                }
                else
                {
                    // last error codes and message set by receive_response
                    return false;
                }
            }
            else
            {
                // last error codes and message set by send_cmd
                return false;
            }
            // return status

            return true;
        }

        /// <summary>
        /// perform ble mode. Returns false if comm error.
        /// </summary>
        /// <remarks>
        /// P1:mode[1B]   idle=0, scan_raw=4, beacon_mfg=5
        /// </remarks>
        /// <param name="mode"></param>
        /// <param name="targetMac"></param>
        /// <param name="timeout">seconds</param>
        /// <param name="detectedMac"></param>
        /// <param name="rcvbeacons"></param>
        /// <returns>false = comm issues. LastErrorCode and LastErrorMessage has reasons</returns>
        public bool PerformBLEMode(uint mode, string targetMac, uint timeoutSecs, ref string detectedMac, ref uint rcvbeacons)
        {
            bool status = true;
            byte[] cmd = new byte[10];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;
            byte[] macarray = new byte[6];
            string lclmac;

            detectedMac = string.Empty;                     // just in case nothing is found
            rcvbeacons = 0;                                 // and no beacons found

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_PERFORM_BLE_MODE;
            cmd[2] = (byte)mode;
            if (targetMac.Length != 0)
            {
                for (int i = 0; i < 6; i++)
                {
                    cmd[3 + i] = (byte)Int32.Parse(targetMac.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
                }
            }
            cmd[9] = (byte)timeoutSecs;

            // send it
            status = send_cmd(cmd);     // if there is a error, LastErrorCode and LastErrorMessage already set

            // verify response
            if (status)
            {
                if ((status = receive_response(ref cmdId, ref cmdStatus, out payload, (int)(timeoutSecs +1) * 1000)))   // give it a extra second to complete
                                                                                                                        // LastErrorxxx set by receive_response
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_PERFORM_BLE_MODE)     // if not the expected response
                    {
                        LastErrorCode = DUTErrorCodes.dutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending BLE MODE command. Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)    // a return of BAD means that no BEACONs where found.
                    {
                        detectedMac = string.Empty;                     // so retrun empty string to indicate nothing found
                        rcvbeacons = 0;                                 // and no beacons found
                        return true;
                    }
                    if (payload.Length > 1) 
                    {
                        Array.Copy(payload, 0, macarray, 0, 6);
                        lclmac = BitConverter.ToString(macarray);
                        detectedMac = lclmac.Replace("-", "");                  // strip out '-'s
                        rcvbeacons = (uint)((payload[7] * 256) + payload[6]);
                    }
                }
            }

            // return status
            return status;
        }

        /// <summary>
        /// Do the radio test
        /// </summary>
        /// <remarks>
        /// P1: destination mac[3B]
        /// resuts
        /// P1: stats
        /// P2: destination mac[3B]
        /// P3: destination LQI[1B}
        /// P4: source LQI[1B]
        /// </remarks>
        /// <param name="destinationMac">mac with no colons</param>
        /// <param name="destLQI"></param>
        /// <param name="sourceLQI"></param>
        /// <returns></returns>
        public bool PerformWtest(string destinationMac, string timeoutms, out string destLQI, out string sourceLQI)
        {
            bool status = true;
            byte[] cmd = new byte[5];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload;;
            int int_timeoutms;

            destLQI = "0";
            sourceLQI = "0";
            if (!int.TryParse(timeoutms, out int_timeoutms))
            {
                LastErrorCode = DUTErrorCodes.dutCmdInputDataFormatError;
                LastErrorMessage = "Timeout must be a int";
                return false;
            }
            if (destinationMac.Length != 12)
            {
                LastErrorCode = DUTErrorCodes.dutCmdInputDataFormatError;
                LastErrorMessage = "Destination Mac must be 12 characters long.";
                return false;
            }
            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_PERFORM_WTEST;
            for (int i = 3; i < 6; i++)
            {
                cmd[i - 1] = (byte)Int32.Parse(destinationMac.Substring(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
            }
            ClearBuffers();
            // send it
            status = send_cmd(cmd);

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload, int_timeoutms))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_PERFORM_WTEST)
                    {
                        LastErrorCode = DUTErrorCodes.dutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending WTEST command. Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = DUTErrorCodes.dutCmdResponseStatusFailed;
                        LastErrorMessage = "Error sending WTEST command. Returned cmd: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                    if (payload.Length == 5)
                    {
                        destLQI = ((int)payload[3]).ToString();
                        sourceLQI = ((int)payload[4]).ToString();
                    }
                    else        // payload was not the correct length
                    {
                        LastErrorCode = DUTErrorCodes.dutCmdResponsePayloadLenWrong;
                        LastErrorMessage = "Error sending WTEST command. Payload len wrong. Should be 5, was " + payload.Length.ToString();
                        return false;

                    }
                }
                else            // timed out
                {
                    return false;
                }
            }
            else
            {
                // last error codes and message set by send_cmd
                return false;
            }
            // return status
            return true;
        }

        /// <summary>
        /// Set FL State.
        /// </summary>
        /// <remarks>
        /// P1: state(0 fixtured, 1 fixtureless
        /// </remarks>
        /// <param name="state"></param>
        /// <returns></returns>
        public bool SetFLState(string stateStr)
        {
            bool status = true;
            byte[] cmd = new byte[2 + 1];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte state = 0;
            byte[] payload; ;

            if (!byte.TryParse(stateStr, out state))
            {
                LastErrorCode = DUTErrorCodes.dutCmdInputDataFormatError;
                LastErrorMessage = Commmands[(int)DVTMANCommands.CMD_SET_FL_STATE].CommandText + ". Value must be a byte.";
                return false;
            }

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_SET_FL_STATE;
            cmd[2] = state;

            // send it
            status = send_cmd(cmd);

            // verify response
            if (status)
            {
                if (receive_response(ref cmdId, ref cmdStatus, out payload))
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_SET_FIXTURELESS_STATE)
                    {
                        LastErrorCode = DUTErrorCodes.dutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_SET_FL_STATE].CommandText + ". Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)
                    {
                        LastErrorCode = DUTErrorCodes.dutCmdResponseStatusFailed;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.CMD_SET_FL_STATE].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                }
            }
            else
            {
                // last error codes and message set by send_cmd
                return false;
            }
            // return status

            return true;
        }

        /// <summary>
        /// Read FL state.
        /// </summary>
        /// <remarks>
        /// P1:mode[1B]   0-fixtured, 1-fixtureless
        /// </remarks>
        /// <param name="mode"></param>
        /// <param name="targetMac"></param>
        /// <param name="timeout">seconds</param>
        /// <param name="detectedMac"></param>
        /// <param name="rcvbeacons"></param>
        /// <returns>false = comm issues. LastErrorCode and LastErrorMessage has reasons</returns>
        public bool DisplayFLState(ref uint flState)
        {
            bool status = true;
            byte[] cmd = new byte[10];
            byte cmdId = 0;
            byte cmdStatus = 0;
            byte[] payload; ;
            byte[] macarray = new byte[6];
            string lclmac;

            flState = 0;                                 // and no beacons found

            // pakage the command
            cmd[0] = DVTMANMessageType;
            cmd[1] = (byte)DVTMANCommands.CMD_DISPLAY_FL_STATE;

            // send it
            status = send_cmd(cmd);     // if there is a error, LastErrorCode and LastErrorMessage already set

            // verify response
            if (status)
            {
                if ((status = receive_response(ref cmdId, ref cmdStatus, out payload, (int)(1) * 1000)))   // give it a extra second to complete
                                                                                                                         // LastErrorxxx set by receive_response
                {
                    if (cmdId != (byte)DVTMANCommands.RSP_DISPLAY_FL_STATE)     // if not the expected response
                    {
                        LastErrorCode = DUTErrorCodes.dutUnexpectedCmdReturn;
                        LastErrorMessage = "Error sending" + Commmands[(int)DVTMANCommands.RSP_DISPLAY_FL_STATE].CommandText + "command. Returned cmd: " + cmdId.ToString("X2");
                        return false;
                    }
                    if (cmdStatus != (byte)DVTMANCommandStatus.GOOD)    // a return of BAD means that no BEACONs where found.
                    {
                        LastErrorCode = DUTErrorCodes.dutCmdResponseStatusFailed;
                        LastErrorMessage = "Error sending " + Commmands[(int)DVTMANCommands.RSP_DISPLAY_FL_STATE].CommandText + ". Returned status: " + cmdId.ToString("X2") + " with BAD status";
                        return false;
                    }
                   
                    flState = payload[0];
                    status = true;
                    
                }
            }

            // return status
            return status;
        }

        //---------------------------------------
        // utils
        //---------------------------------------

        /// <summary>
        /// will send the byte array to the serial port. It will first convert it to the
        /// encoded version.
        /// </summary>
        /// <remarks>
        /// LastErrorCode
        ///  2  ErrorCodes.Program.DUTComError
        /// </remarks>
        /// <param name="cmd">byte array to send to serial port</param>
        /// <returns>true = sent</returns>
        private bool send_cmd(byte[] cmd)
        {
            byte[] convertedCmd;
            byte[] endbyte = { 0 };
            byte[] header = new byte[4];
            byte[] xmitarray;
            byte chksum = 0;
            string statusString;

            // now wrap in the transport layer
            //      uint32_t version    :2	        transport protocol version, currently 0
            //      uint32_t rsvd       :5	        reserved, always set to 0
            //      uint32_t secure     :1	        set if secure payload, clear if payload is not secure
            //      uint32_t sequence_num: 8	    incremented after each packet transmit
            //                                      used by receiver to determine lost packets
            //      uint32_t payload_size:	:8;     number of bytes in the transport payload
            //      uint32_t ck_sum :8	            checksum
            header[0] = 0;
            header[1] = packetTxSeqNumber++;
            header[2] = (byte)cmd.Length;
            header[3] = 0;
            xmitarray = new byte[4 + cmd.Length];
            Buffer.BlockCopy(header, 0, xmitarray, 0, 4);
            Buffer.BlockCopy(cmd, 0, xmitarray, 4, cmd.Length);
            foreach (byte item in xmitarray)
            {
                chksum += item;
            }
            xmitarray[3] = (byte)~chksum;       // ones complement

            convertedCmd = COBSCodec.encode(xmitarray);
            Array.Resize<byte>(ref convertedCmd, convertedCmd.Length + 1);
            convertedCmd[convertedCmd.Length - 1] = 0;                      // add trailing 0

            try
            { 
                if (serial.IsOpen)
                {
                    serial.Write(convertedCmd, 0, convertedCmd.Length);
                    statusString = DateTime.Now.ToString("HH:mm:ss.fff") + "->" + BitConverter.ToString(convertedCmd) + "\n";
                }
                else
                {
                    LastErrorMessage = "Error sending data. Port is closed";
                    LastErrorCode = FixtureErrorCodes.DUTConsoleNotOpen;
                    return false;
                }
            }
            catch (Exception ex)
            {
                LastErrorMessage = "Error sending data. " + ex.Message;
                LastErrorCode = FixtureErrorCodes.DUTConsoleWriteError;
                return false;
            }

            //if (DisplaySendChar)
            //{
            //    UpdateStatusWindow(statusString, IsThisDUTConsole ? StatusType.serialSend :StatusType.snifferSend);
            //}
            if (dumpFileName != string.Empty)
            {
                lock (flocker)
                {
                    using (System.IO.StreamWriter file = new System.IO.StreamWriter(dumpFileName, true))
                    {
                        file.Write(statusString);
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Receive a response from a sent command. It will read until a 0 byte, or timeout(CommandRecieveTimeoutMS).
        /// </summary>
        /// <remarks>
        /// The decoded response will always have the following format:
        ///     byte 0 - command ID
        ///     byte 1 - command status
        ///     byte 2 and on - payload. the payload could be 0 bytes
        ///     
        /// LastErrorCode
        ///  16  ErrorCodes.Program.CommandTimeout
        ///  19  ErrorCodes.Program.CmdResponsePayloadLenWrong
        ///  23  ErrorCodes.Program.CmdPacketFormatError
        ///  22  ErrorCodes.Program.CmdResponseHeaderLenWrong
        ///  21  ErrorCodes.Program.CmdMsgTypeNotDVTMAN
        ///  20  ErrorCodes.Program.CmdPacketCheckSumError
        /// </remarks>
        /// <param name="payloadLenght = expected payload length"></param>
        /// <returns></returns>
        private bool receive_response(ref byte cmdId, ref byte cmdStatus, out byte[] payload)
        {
            return receive_response(ref cmdId, ref cmdStatus, out payload, CommandRecieveTimeoutMS);
        }

        /// <summary>
        /// recicve a response from the port. Will decode the valid packet. 
        /// </summary>
        /// <param name="cmdId"></param>
        /// <param name="cmdStatus"></param>
        /// <param name="payload">valid payload. null if invalid</param>
        /// <param name="timeoutMs"></param>
        /// <returns>true = valid packet</returns>
        private bool receive_response(ref byte cmdId, ref byte cmdStatus, out byte[] payload, int timeoutMs)
        {
            DateTime starttime = DateTime.Now;
            byte[] fullPacket;
            bool status = true;
            string fmsg = string.Empty;
            bool foundend = false;
            int bytesRead = 0;
            int endIndex = 0;


            LastErrorCode = 0;
            LastErrorMessage = string.Empty;
            payload = null;

            if (!serial.IsOpen)
            {
                LastErrorCode = FixtureErrorCodes.InstrumentNotConnected;
                LastErrorMessage = "Error: Serial port not open when waiting for receive response";
                return false;
            }
            // read till 0 found or timeout
            do
            {
                try
                {
                    bytesRead = serial.BaseStream.Read(packetBuffer, bufferSaveIndex, 255 - bufferSaveIndex);
                    Debug.Print("bytes read:" + bytesRead + "\n");
                }
                catch (Exception ex)
                {
                    LastErrorCode = DUTErrorCodes.dutCommandTimeout;
                    LastErrorMessage = "Error: " +  ex.Message + " waiting for receive response";
                    status = false;
                    Thread.Sleep(50);
                }
                bufferSaveIndex += bytesRead;
                if (bufferSaveIndex != 0)     // something in read buffer
                {
                    for (int i = 0; i < bufferSaveIndex; i++)
                    {
                        if (packetBuffer[i] == 0)           // if found end of packet
                        {
                            endIndex = i;
                            foundend = true;
                            break;
                        }
                    }
                }
                
                if (foundend)                            // if found the end of a packet
                {
                    packetRxSeqNumber++;                                    // inc the rcv packet counter
                    byte[] savebuf = new byte[endIndex + 1];             // make the correct size buffer
                    Array.Copy(packetBuffer, savebuf, endIndex + 1);        // copy the data. it start at begining of buffer
                    packetList.Add(savebuf);                                // save in list
                    // get rid of data in the master buffer. at this point 
                    // endIndex is where the zero is.
                    Array.Copy(packetBuffer, endIndex + 1, packetBuffer, 0, bufferSaveIndex - endIndex - 1);
                    bufferSaveIndex -= endIndex + 1;                     // fix where to put new data
                    fmsg = DateTime.Now.ToString("HH:mm:ss.fff(" + packetList.Count.ToString() + ")") + "<-";
                    //if (DisplayRcvChar)         // if display window is enabled
                    //{
                    //    UpdateStatusWindow(fmsg + BitConverter.ToString(savebuf) + "\n", IsThisDUTConsole ? StatusType.serialRcv : StatusType.snifferRcv);
                    //}
                }

            } while (!foundend & (starttime.AddMilliseconds(timeoutMs) > DateTime.Now));     // keep trying if no packet and not timed out

            if (packetList.Count != 0)          // have something
            {
                fullPacket = packetList.First();    // get the packet
                packetList.RemoveAt(0);         // pop it off the list
                status = (decodePacket(fullPacket, ref cmdId, ref cmdStatus, out payload));     // if decode fails, LastErrorxxx is set by decodePacket
                //if (DisplayRcvChar)
                //{
                //    if (payload == null)
                //        UpdateStatusWindow("Rcv id=" + cmdId.ToString() + " status=" + cmdStatus.ToString() + " Payload= none\n", IsThisDUTConsole ? StatusType.serialRcv : StatusType.snifferRcv);
                //    else
                //        UpdateStatusWindow("Rcv id=" + cmdId.ToString() + " status=" + cmdStatus.ToString() + " Payload= " + BitConverter.ToString(payload) + "\n", IsThisDUTConsole ? StatusType.serialRcv : StatusType.snifferRcv);
                //}
                fmsg = DateTime.Now.ToString("HH:mm:ss.fff") + "<-";
                if (dumpFileName != string.Empty)
                {
                    lock (flocker)
                    {
                        using (System.IO.StreamWriter file = new System.IO.StreamWriter(dumpFileName, true))
                        {
                            file.Write(fmsg + BitConverter.ToString(fullPacket) + "\n");
                        }
                    }
                }
            }
            else
            {
                LastErrorCode = DUTErrorCodes.dutCommandTimeout;
                LastErrorMessage = "Error: Timeout waiting for receive response";
                status = false;
            }

            return status;
        }


        /// <remarks>
        /// transport layer
        ///     uint32_t version    :2	        transport protocol version, currently 0
        ///      uint32_t rsvd       :5	        reserved, always set to 0
        ///      uint32_t secure     :1	        set if secure payload, clear if payload is not secure
        ///      uint32_t sequence_num: 8	    incremented after each packet transmit
        ///                                      used by receiver to determine lost packets
        ///      uint32_t payload_size:	:8;     number of bytes in the transport payload
        ///      uint32_t ck_sum :8	            checksum
        ///     byte[] packet
        ///     
        /// The packet will always have the following format:
        ///     byte 0 - Message type -> 0x57 for DVTMAN commands
        ///     byte 1 - Message ID
        ///     byte 2 - Message status
        ///     byte 3 and on - payload. the payload could be 0 bytes
        ///     
        /// LastErrorCode
        ///  19  ErrorCodes.Program.CmdResponsePayloadLenWrong
        ///  23  ErrorCodes.Program.CmdPacketFormatError
        ///  22  ErrorCodes.Program.CmdResponseHeaderLenWrong
        ///  21  ErrorCodes.Program.CmdMsgTypeNotDVTMAN
        ///  20  ErrorCodes.Program.CmdPacketCheckSumError
        /// </remarks>
        private bool decodePacket(byte[] packet, ref byte cmdId, ref byte cmdStatus, out byte[] payload)
        {
            byte[] datapacket;                      // data part of the response
            byte[] header = new byte[4];
            byte[] commandresponse;                 // decoded response
            byte chksum = 0;

            LastErrorCode = 0;
            LastErrorMessage = string.Empty;

            payload = null;
            if (packet.Length == 0)                 // if nothing 
            {
                LastErrorCode = DUTErrorCodes.dutCmdResponsePayloadLenWrong;
                LastErrorMessage = "Error: Decoding packet. Found 0 bytes";
                return false;
            }

            byte[] decodepacket = new byte[packet.Length - 1];      // need to remove the trailing 0
            Array.Copy(packet, decodepacket, packet.Length - 1);
            try
            {
                commandresponse = COBSCodec.decode(decodepacket);
            }
            catch (Exception ex)   // problem with the packet format
            {
                LastErrorCode = DUTErrorCodes.dutCmdPacketFormatError;
                LastErrorMessage = "Error: Error trying to decode paccket. " + ex.Message;
                return false;
            }
            if (commandresponse.Length < 7)             // if it does not have the minimum number of bytes
            {
                LastErrorCode = DUTErrorCodes.dutCmdResponseHeaderLenWrong;
                LastErrorMessage = "Error: Packet has less than 7 bytes. Found " + commandresponse.Length.ToString() + " bytes";
                return false;
            }
            foreach (byte item in commandresponse)      // get the checksum
            {
                chksum += item;
            }
            if (chksum != 0xFF)           // check the checksum
            {
                LastErrorCode = DUTErrorCodes.dutCmdPacketCheckSumError;
                LastErrorMessage = "Error: Checksum is wrong";
                return false;
            }
            if (commandresponse[4] != DVTMANMessageType)    // make sure it is the correct message type
            {
                LastErrorCode = DUTErrorCodes.dutCmdMsgTypeNotDVTMAN;
                LastErrorMessage = "Error: Incorrect message type. Found:" + commandresponse[4].ToString();
                return false;
            }
            // make sure the specified payload lenght match the actual payload len
            datapacket = new byte[commandresponse.Length - 4];
            Array.Copy(commandresponse, 4, datapacket, 0, commandresponse.Length - 4);
            if (datapacket.Length != commandresponse[2])
            {
                LastErrorCode = DUTErrorCodes.dutCmdResponsePayloadLenWrong;
                LastErrorMessage = "Error: Payload lenght wrong. Should be " + commandresponse[2].ToString() + " but found " + datapacket.Length + "bytes";
                return false;
            }
            // OK. Everything looks good now
            cmdId = datapacket[1];              // get command ID
            cmdStatus = datapacket[2];          // get status
            payload = new byte[datapacket.Length - 3];
            Array.Copy(datapacket, 3, payload, 0, datapacket.Length - 3);
            return true;
        }

        //--------------------------
        // Events
        //---------------------------

        private void HandleError(object sender, SerialErrorReceivedEventArgs e)
        {
            Console.Write("Serial error: " + e.EventType.ToString() + "\n");
            //UpdateStatusWindow("Serial error: " + e.EventType.ToString() + "\n");
        }
    }
}
