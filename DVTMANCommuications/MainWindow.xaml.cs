﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;
using System.ComponentModel;

namespace DVTMANCommuications
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        SerialPortControls.SerialPortInfo SelectedComPort;
        private string cmdResults = string.Empty;
        public ResultBoxClass resultString = new ResultBoxClass();
        string prgver = string.Empty;

        public MainWindow()
        {
            InitializeComponent();

            List<commandClass> cmdList = new List<commandClass>();
            // set up the listbox
            foreach (var item in Commmands)
            {
                if (item.Display)
                    cmdList.Add(item);
            }
            lv_commandList.ItemsSource = cmdList;
            this.tbCommandResults.DataContext = resultString;
            tbPrgVer.Text = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Major + "." + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Minor;
            
        }

        ~MainWindow()
        {
            CloseComm();
        }

        private void Connect_Button_Click(object sender, RoutedEventArgs e)
        {
            PortName = ComInfo.SelectedPort.Name;
            LastErrorMessage = string.Empty;
            LastErrorCode = 0;
            if ((string)ConnectButton.Content == "Connect")     //if waiting to connect
            {
                OpenComm();
                if (LastErrorCode != 0)
                {
                    MessageBox.Show("Problem opening comm port.\n" + LastErrorMessage);
                    CloseComm();
                    PortName = string.Empty;
                }
                else
                {
                    ConnectButton.Background = Brushes.Green;
                    ConnectButton.Content = "Disconnect";
                }
            }
            else
            {
                if (IsOpen())
                    CloseComm();
                if (LastErrorCode != 0)
                {
                    MessageBox.Show("Problem closing comm port.\n" + LastErrorMessage);
                }
                ConnectButton.Background = Brushes.Gray;
                ConnectButton.Content = "Connect";
                PortName = string.Empty;
            }

        }


        private void RunCommand_Button_Click(object sender, RoutedEventArgs e)
        {
            bool result;
            bool hasParams = false;         // set to true if there are input params found. Parameters[i].ReturnValue = false
            commandClass selectedCmd;
            string results = string.Empty;

            if (PortName == string.Empty)
            {
                MessageBox.Show("No com port has been opened.");
                return;
            }
            // get the data for the selected command
           selectedCmd = (commandClass)lv_commandList.SelectedItem;
            if (selectedCmd.CommandFunctionName == null)
            {
                MessageBox.Show("Function is not impemented.");
                return;
            }
            Type t = this.GetType();
            MethodInfo method = t.GetMethod(selectedCmd.CommandFunctionName);

            if (selectedCmd.Parameters == null)     // there are no parameters defined
            {
                result = (bool)method.Invoke(this, null);
                resultString.result = "Command Finished";
            }
            else                                    // there are parameters
            {
                // make the window for the selected command
                CommandParameters.GetCommandParameters win = new CommandParameters.GetCommandParameters(selectedCmd);
                // get the parameters for this command
                // put them a array to pass to the function
                object[] functionParams = new object[win.cmdDef.Parameters.Count];
                for (int i = 0; i < win.cmdDef.Parameters.Count; i++)
                {
                    functionParams[i] = win.cmdDef.Parameters[i].ParaData;
                    if (!win.cmdDef.Parameters[i].ReturnValue)
                        hasParams = true;
                }
                if (hasParams)
                    win.ShowDialog();       // get the data for the input parameters
                resultString.result = "Starting command";
                tbCommandResults.GetBindingExpression(TextBlock.TextProperty).UpdateTarget();
                try
                {
                    // get parameters in array
                    for (int i = 0; i < win.cmdDef.Parameters.Count; i++)
                    {
                        functionParams[i] = win.cmdDef.Parameters[i].ParaData;
                    }
                    result = (bool)method.Invoke(this, functionParams);
                    if (result)     // if good call
                    {
                        for (int j = 0; j < win.cmdDef.Parameters.Count; j++)
                        {
                            if (win.cmdDef.Parameters[j].ReturnValue)
                            {
                                results += win.cmdDef.Parameters[j].ParaText + ":" + functionParams[j] + "\n";
                            }
                        }
                    }
                    resultString.result = results + "\nCommand finished.";
                }
                catch (Exception ex)
                {
                    win.Close();
                    MessageBox.Show("Problem calling " + selectedCmd.CommandFunctionName + ". \n" + ex.InnerException.TargetSite.Name + ":" + ex.InnerException.Message);
                    return;
                }
                win.Close();
            }
            if (!result)
                resultString.result =  "Error doing command " + selectedCmd.CommandText + ". \n" + LastErrorMessage;
            tbCommandResults.GetBindingExpression(TextBlock.TextProperty).UpdateTarget();
        }

        private void ClearSerialBuffer_Button_Click(object sender, RoutedEventArgs e)
        {
            if (serial == null)
                resultString.result = "Error. Com port not open.";
            else if (!IsOpen())
                resultString.result = "Error. Com port not open.";
            else
            {
                ClearBuffers();
                resultString.result = "Buffers cleared.";
            }
            tbCommandResults.GetBindingExpression(TextBlock.TextProperty).UpdateTarget();
        }
    }
    public class ResultBoxClass
    {
        public string result { set; get; }
    }
}